#########################################
# Roboty Mobilne 2						#
# Pawel Michniewicz, 235713				#
#	#	#	#	#	#	#	#	#	#	#
# Labolatorium 2						#
# Zadania b), d), e) 					#
# Metoda pol potencjalow				#
#########################################



import numpy as np
import math
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import matplotlib.animation as animation 	
from matplotlib.path import Path
from matplotlib.patches import PathPatch

from random import randint 		# funkcja generujaca losowa liczbe typu int
from numpy import linalg as LA	# funkcja linalg.norm() z pakietu pozwala na policzenie normy

# Funkcja wywolywana w trakcie tworzenia animacji. Wyswietla kolejne punkty ze sciezki
# laczac je w linie
def update(i):
    line.set_data([x for x in path[0:i,0]], [y for y in path[0:i,1]])
    return line

# Funkcja liczaca wartosc sily wypadkowej w punkcie o wspolrzednych [x,y].
# Uwzglednia przeszkody znajdujace sie w zakresie dr
def ForceAtPoint(x, y, dr):
	Z_temp = np.zeros([x_size*2+1, y_size*2+1])
	Fo = np.zeros(howmany_obstacles)
	Fo_sum = 0

	Fp = -kp*(LA.norm([x-finish_point[0], y-finish_point[1]]))
		
	for k in range(howmany_obstacles) :
		qro = LA.norm([x-obst_vect[k][0], y-obst_vect[k][1]])
			
		if qro == 0:
			qro = 0.1
		if qro <= dr:
			Fo[k] = -ko[k]*(1/qro - 1/dr)*(1/(qro**2))
		else: 
			Fo[k] = 0
		Fo_sum += Fo[k]

	return Fp + Fo_sum


# Funkcja wyznaczajaca kolejny punkt ruchu robota podczas symulacji, na podstawie wartosci sily wypadkowej
# w aktualnym punkcie z uwzglednieniem przezszkod mieszczacych sie w zakresie dr.
# Wektor considered_points o rozmiarze 1x8 sluzy do okreslenia, ktore z otaczajacych robota punktow nalezy uwzglednic 
# podczas wyznaczania kolejnego punktu ruchu. Numeracja tych punktow przedstawia sie nastepujaco:
#
#	-------------
#	| 5 | 3 | 0 |
#	-------------
#	| 6 | R | 1 |
#	-------------
#	| 7 | 4 | 2 |
# 	-------------
#
# Wartosc -9 w wektorze considered_points oznac, ze dany punkt nie bedzie rozwazany. 
def chooseNextPoint(considered_points=np.zeros(8), current_position=np.zeros(2), dr=10.0):
	near_forces = np.zeros([8,3])
	next_position = np.zeros(3)
	next_position[0] = -1000000

	near_forces[0,:] = [ForceAtPoint(current_position[0]+1, current_position[1]+1, dr), current_position[0]+1, current_position[1]+1]
	near_forces[1,:] = [ForceAtPoint(current_position[0]+1, current_position[1], dr), current_position[0]+1, current_position[1]]
	near_forces[2,:] = [ForceAtPoint(current_position[0]+1, current_position[1]-1, dr), current_position[0]+1, current_position[1]-1]	
	near_forces[3,:] = [ForceAtPoint(current_position[0], current_position[1]+1, dr), current_position[0], current_position[1]+1]
	near_forces[4,:] = [ForceAtPoint(current_position[0], current_position[1]-1, dr), current_position[0], current_position[1]-1]
	near_forces[5,:] = [ForceAtPoint(current_position[0]-1, current_position[1]+1, dr), current_position[0]-1, current_position[1]+1]
	near_forces[6,:] = [ForceAtPoint(current_position[0]-1, current_position[1], dr), current_position[0]-1, current_position[1]]
	near_forces[7,:] = [ForceAtPoint(current_position[0]-1, current_position[1]-1, dr), current_position[0]-1, current_position[1]-1]

	for i in considered_points:
		if i != -9 and next_position[0] <= near_forces[i,0]:
			next_position[0] = near_forces[i,0]
			next_position[1] = near_forces[i,1]
			next_position[2] = near_forces[i,2]
	return next_position


# Parametry sceny:
delta = 0.1				
x_size = 10				# rozmiar sceny
y_size = 10				#
howmany_obstacles = 20
x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)


start_point=np.array([-10,randint(-y_size,y_size)], dtype='float32')	# losowy punkt startowy
finish_point=np.array([10,randint(-y_size,y_size)], dtype='float32')	# losowy punkt celu

obst_vect = np.zeros([howmany_obstacles,2], dtype='float32')	# generacja przeszkod o losowych wspolrzednych
for i in range(howmany_obstacles):								#
	obst_vect[i] = [randint(-10,10),randint(-10,10)]			#

ko = np.zeros(howmany_obstacles)		# wprost proporcjonalna regulacja wartosci sily odpychajacej kazdej 
for i in range(howmany_obstacles):		# z przeszkod z osobna
	ko[i] = 0.017						#

d0 = 10.0								# zasieg wykrywalnosci przeszkod

kp = 1.0								# wprost proporcjonalna regulacja wartosci sily przyciagajacej do 
										# punktu celu


# Macierz wartosci wypadkowych sil w kazdym punkcie sceny (w dalszej czesci transponowana i nakladana na wykres funkcja imshow):
Z = np.zeros([x_size*2+1, y_size*2+1])
for i in range(x_size*2+1) :
	for j in range(y_size*2+1) :
		Z[i, j] = ForceAtPoint(i-x_size, j-y_size, d0)

# Parametry symulacji:
current_point = start_point.copy();						# aktualne polozenie robota

path = np.empty([0,2], dtype='int32')					# sciezka od punktu poczatkowego do punktu celu
path = np.concatenate((path, [current_point]), axis=0)	#

number_of_iteriation = 0								# zmienne uzywane do kontroli ilosci iteracji petli
max_iterations = 40										#

next_point = np.zeros(3)								# wektor przechowujacy wspolrzedne oraz wartosc sily wypadkowej 
														# nastepnego punktu ruchu

dro = d0												# zakres wykrywania przeszkod

# GLOWNA PETLA
# Program wyznacza kolejne punkty ruchu na postawie sily wypadkowej i dodaje je do sciezki,
# az osiagnie punkt docelowy, lub wykona maksymalna liczbe iteracji
while not np.array_equal(current_point, finish_point):
	if current_point[0] <= -x_size:
		if current_point[1] >= y_size:
			next_point = chooseNextPoint([-9, 1, 2, 3, -9, -9, -9, -9], current_point, dro)
		elif current_point[1] <= -y_size:
			next_point = chooseNextPoint([0, 1, -9, -9, 4, -9, -9, -9], current_point, dro)
		else:
			next_point = chooseNextPoint([0, 1, 2, 3, 4, -9, -9, -9], current_point, dro)
	elif current_point[0] >= x_size:
		next_point = chooseNextPoint([-9, -9, -9, 3, 4, 5, 6, 7], current_point, dro)
	elif current_point[1] <= -y_size:
		next_point = chooseNextPoint([0, 1, -9, 3, -9, 5, 6, -9], current_point, dro)
	elif current_point[1] >= y_size:
		next_point = chooseNextPoint([-9, 1, 2, -9, 4, -9, 6, 7], current_point, dro)
	else:
		next_point = chooseNextPoint([0, 1, 2, 3, 4, 5, 6, 7], current_point, dro)
		
	current_point[0] = int(next_point[1])
	current_point[1] = int(next_point[2])
	
	path = np.concatenate((path, [current_point]), axis=0)
	
	number_of_iteriation += 1
	if number_of_iteriation >= max_iterations:
		break;


# Tworzenie wykresu
fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z.T, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "bs", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)

# Tworzenie animacji
line, =ax.plot([], [], linewidth=3)
ani = animation.FuncAnimation(fig, update, interval=10,  save_count=1)

plt.show()































