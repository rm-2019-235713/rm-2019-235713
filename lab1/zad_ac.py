#########################################
# Roboty Mobilne 2						#
# Pawel Michniewicz, 235713				#
#	#	#	#	#	#	#	#	#	#	#
# Labolatorium 1						#
# Zadania a), c)						#
#########################################

import numpy as np
import math
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch

from random import uniform		# funkcja generujaca losowa liczbe typu float
from numpy import linalg as LA 	# funkcja linalg.norm() z pakietu pozwala na policzenie normy

delta = 0.01
x_size = 10
y_size = 10
howmany_obstacles = 4

ko = np.array([0.5,0.5,0.5,0.5]) 	# wprost proporcjonalna regulacja wartosci sily odpychajacej kazdej 
									# z przeszkod z osobna

d0 = 20								# odwrotnie proporcjonalna regulacja wartosci sily odpychajacej 
									# o zasiegu globalnym, dla kazdej z przeszkod

kp = 2								# wprost proporcjonalna regulacja wartosci sily przyciagajacej do 
									# punktu celu

obst_vect = np.zeros((howmany_obstacles,2))	# generacja pustego (wypelnionego zerami) wektora przeszkod

for i in range(howmany_obstacles):
	obst_vect[i] = [uniform(-10,10),uniform(-10,10)]	# wypelnienie wektora przeszkod losowymi wspolzednymi

start_point=np.array([-10,uniform(-y_size,y_size)])	# losowy punkt startu
finish_point=np.array([10,uniform(-y_size,y_size)]) # losowy punkt celu

# Parametry sceny wg szablonu
x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)

Z = np.zeros((x_size*2, y_size*2))	# Generowanie macierzy wartosci sil wypadkowych, wypelnionej zerami

# Generowanie macierzy wartosci wypadkowych wektorow sil dla kazdego punktu
for i in range(x_size*2) :
	for j in range(y_size*2) :
		Fo = np.zeros(4)	# Wektor wszystkich sil odpychajacych od przeszkod
		Fo_sum = 0			# Zmienna przechowujaca sume wartosci sil odpychajacych
		
		curr_x = i-x_size+0.5	# wspolrzedna na OX w aktualnym punkcie
		curr_y = j-y_size+0.5	# wspolrzedna na OY w aktualnym punkcie

		Fp = kp*(LA.norm([curr_x-finish_point[0], curr_y-finish_point[1]])) # Wartosc sily przyciagajacej 
																			# ze wzoru z instrukcji
		
		# Liczenie sumy wartosci sil odpychajacych ze wzoru z instrukcji
		for k in range(howmany_obstacles) :
			qro = LA.norm([curr_x-obst_vect[k][0], curr_y-obst_vect[k][1]])
			if qro <= d0 :
				Fo[k] = -ko[k]*(1/qro - 1/d0)*(1/(qro**2))
			else: 
				Fo[k] = 0
			Fo_sum += Fo[k]

		Z[j][i] = Fp + Fo_sum	# Liczenie wartosci wypadkowej sily w aktualnym punkcie


# Generowanie rysunku wg szablonu
fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
