#########################################
# Roboty Mobilne 2						#
# Pawel Michniewicz, 235713				#
#	#	#	#	#	#	#	#	#	#	#
# Labolatorium 3						#
# Algorytm S-shape	 					#
#########################################

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import matplotlib.patches as patches
import matplotlib.animation as animation
from matplotlib.path import Path
from scipy.spatial import distance

def add_obstacle(x, y, width, height):
    for i in np.arange(_scene.shape[0]):
        for j in np.arange(_scene.shape[1]):
            if j >= x and j <= x + width and i >= y and i <= y + height:
                _scene[j][i] = -1

def add_position(x,y):
    if x < 0 or x >= x_size or y < 0 or y >= y_size or scene[x][y] == -1:
        raise Exception("Wjechales w przeszkode: (" + str(x) + "," + str(y) + ")")
    global x_pos
    global y_pos
    positions[0].append(x)
    positions[1].append(y)
    scene[x][y] = 1
    x_pos = x
    y_pos = y

def move_x(val):
    move(val, 1, 0)


def move_y(val):
    move(val, 0, 1)

def move(val, x, y):
    for i in np.arange(0, abs(val)):
        if val > 0:
            add_position(x_pos + x, y_pos + y)
        else:
            add_position(x_pos - x, y_pos - y)

def update(i):
    line.set_data([x+0.5 for x in positions[0][0:i]], [y+0.5 for y in positions[1][0:i]])
    return line

def coverage():
    full_size = len(scene.flatten())
    obst_size = np.count_nonzero(scene == -1)
    cleaned_size = np.count_nonzero(scene == 1)
    dirty_size = np.count_nonzero(scene == 0)
    coveraged_size = cleaned_size/(full_size - obst_size) * 100

    print("Koncowe polozenie: (" + str(x_pos) + ", " + str(y_pos)+ ")")
    print("Pelna scena: " + str(full_size))
    print("Przeszkody: " + str(obst_size))
    print("Posprzatane pole: " + str(cleaned_size))
    print("Zasmiecone pole: " + str(dirty_size))
    print("Pokrycie: " + str(round(coveraged_size,2)) + "%")

delta = 1
x_size = 40
y_size = 40
x_pos = 0
y_pos = 25
positions=[[],[]]
x = y = np.arange(0, 40.0, delta)
X, Y = np.meshgrid(x, y)
_scene = X*0
_scene[0][25]= 1 #punkt poczatkowy
add_obstacle(0,0,4,16)
add_obstacle(25,32,15,8)
add_obstacle(24,12,4,12)
scene = _scene.copy()
add_position(x_pos, y_pos)


# Uzupelnic kodem
animacja=True
count = 1

which_algorithm = 1 # 0 -> algorytm z tablicy 
					# 1 -> zmodyfikowany algorytm


orientation = 0	# 0 ->  up
				# 1 ->  down
				# 2 ->  right
				# 3 ->  left

def move_forward():
	if orientation == 0:
		move_y(1)
	elif orientation == 1:
		move_y(-1)
	elif orientation == 2:
		move_x(1)
	elif orientation == 3:
		move_x(-1)

def move_backwards():
	if orientation == 0:
		move_y(-1)
	elif orientation == 1:
		move_y(1)
	elif orientation == 2:
		move_x(-1)
	elif orientation == 3:
		move_x(1)

def turn_right():
	global orientation
	if orientation == 0:
		orientation = 2
	elif orientation == 1:
		orientation = 3
	elif orientation == 2:
		orientation = 1
	elif orientation == 3:
		orientation = 0

def turn_left():
	global orientation
	if orientation == 0:
		orientation = 3
	elif orientation == 1:
		orientation = 2
	elif orientation == 2:
		orientation = 0
	elif orientation == 3:
		orientation = 1

if which_algorithm == 0:
	orientation = 0
	iteriations = 0
	while iteriations < 5000:
		iteriations += 1
		try:
			move_forward()
		except Exception: 					# if collision
			if count < 40:				
				if count%2 == 1:	
					turn_right()		
					try:
						move_forward()
					except Exception:		# if collision
						move_backwards()
					finally:
						turn_right()
			
				else: 							
					turn_left()
					move_forward()
					turn_left()
			count += 1	

elif which_algorithm == 1:
	iteriations = 0
	while iteriations < 5000:
		iteriations += 1
		try:
			move_forward()
		except Exception: 				# if collision
			if orientation == 0 or orientation == 3:	
				turn_right()		
				try:
					move_forward()
				except Exception:		# if collision
					#move_backwards()
					turn_right()
					#move_forward()
				finally:
					turn_right()
			
			elif orientation == 1 or orientation == 2: 							
				turn_left()
				try:
					move_forward()
				except Exception:
					#move_backwards()				
					turn_left()
					#move_forward()
				finally:
					turn_left()
		

#######################

_scene[x_pos][y_pos] = 1 
_scene = np.where(scene == 0, -0.5, _scene)

fig = plt.figure()
ax = fig.add_subplot(111)
line, =ax.plot([], [], linewidth=3)
plt.imshow(_scene.transpose(), cmap=cm.RdYlGn, origin='lower', extent=[0, x_size, 0, y_size])
ax.set_title("‘S’ shape pattern algorithm")
plt.grid(True)
if animacja:
    ani = animation.FuncAnimation(fig, update, interval=10,  save_count=1)
else:
    plt.plot(positions[0], positions[1], linewidth=3)
coverage()

plt.show()


